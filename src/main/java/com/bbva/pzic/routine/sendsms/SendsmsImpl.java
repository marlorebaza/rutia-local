package com.bbva.pzic.routine.sendsms;

import com.bbva.jee.arq.spring.core.servicing.interfaces.ISendSMS;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.List;

@Component
public class SendsmsImpl implements ISendSMS {

    private static final Log LOG = LogFactory.getLog(SendsmsImpl.class);

    @Override
    public void sendSMS(String s, String s1, String s2, List<Object> list, String s3, String s4, String s5, Method method) {
        LOG.info(".......... [RUTINA LOCAL OTP-SMS] dentro de SendsmsImpl.sendSMS() ..........");
        LOG.error(".......... [RUTINA LOCAL OTP-SMS] dentro de SendsmsImpl.sendSMS() ..........");
        LOG.warn(".......... [RUTINA LOCAL OTP-SMS] dentro de SendsmsImpl.sendSMS() ..........");
        LOG.debug(".......... [RUTINA LOCAL OTP-SMS] dentro de SendsmsImpl.sendSMS() ..........");
    }
	
}

